# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Added

- add submode for inputs

### Changed

- test: use personal docker registry

### Fixed

- change apt key

### Removed

- support debian 11

## v1.0.0 - 2021-08-20

### Added

- first version
