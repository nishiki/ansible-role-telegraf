# Ansible role: Telegraf

[![Version](https://img.shields.io/badge/latest_version-1.0.0-green.svg)](https://code.waks.be/nishiki/ansible-role-telegraf/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-telegraf/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-telegraf/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-telegraf/actions?workflow=molecule.yml)

Install and configure Telegraf

## Requirements

- Ansible >= 2.9
- Debian
  - Bookworm

## Role variables

- `telegraf_config` - hash with the general configuration (see [telegraf documentation](https://docs.influxdata.com/telegraf/latest/administration/configuration/))

```
  agent:
    interval: 10s
    round_interval: true
    metric_batch_size: 1000
    metric_buffer_limit: 10000
    collection_jitter: 0s
    flush_interval: 10s
    flush_jitter: 0s
    precision: ''
    hostname: ''
    omit_hostname: false
```

- `telegraf_inputs` - hash with inputs configuration

```
  cpu:
    percpu: true
    totalcpu: true
    collect_cpu_time: false
    report_active: false
  postgresql_extensible:
    address: "host=localhost"
    submodule:
      - name: query
        version = 901
        sqlquery = "SELECT pg_is_in_recovery()::INT"
        withdbname = false
        tagvalue = ""
```

- `telegraf_outputs` - hash with outputs configuration

```
  file:
    files: ['stdout', '/tmp/metrics.out']
```

- `telegraf_user_groups` - array with the telegraf unix groups

```
  - docker
  - admin
```

- `telegraf_proxy_url` - set a proxy url for http and https requests
- `telegraf_proxy_ignore` - array with ignore host or subnet

```
  - localhost
  - 10.0.0.0/8
```

## How to use

```
- hosts: server
  roles:
    - telegraf
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-docker docker ansible-lint pytest-testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2021 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
