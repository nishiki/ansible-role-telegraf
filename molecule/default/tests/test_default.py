import testinfra.utils.ansible_runner
import time

def test_packages(host):
  package = host.package('telegraf')
  assert package.is_installed

def test_config_file(host):
  path = host.file('/etc/telegraf/telegraf.conf')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'telegraf'
  assert path.mode == 0o640
  assert path.contains('files = \\["stdout", "/tmp/metrics.out"\\]')

def test_config_file(host):
  path = host.file('/etc/default/telegraf')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('https_proxy=http://127.0.0.1:3128')

def test_service(host):
  service = host.service('telegraf')
  assert service.is_running
  assert service.is_enabled
 
def test_output(host):
  time.sleep(15)
  path = host.file('/tmp/metrics.out')
  assert path.exists
  assert path.is_file
  assert path.contains('idle')
